libmoox-types-mooselike-perl (0.29-1.1apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 11 Mar 2021 17:09:40 +0000

libmoox-types-mooselike-perl (0.29-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Mon, 28 Dec 2020 13:56:54 +0100

libmoox-types-mooselike-perl (0.29-1co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Sat, 20 Feb 2021 01:18:22 +0000

libmoox-types-mooselike-perl (0.29-1) unstable; urgency=medium

  * Import upstream version 0.29.
  * Add new upstream copyright holder.
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Thu, 09 Jul 2015 20:54:44 +0200

libmoox-types-mooselike-perl (0.28-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.28
  * Mark package as autopkgtest-able.
  * Bump {build,runtime}-dependency on libmoo-perl to >= 1.004002.
  * Bump {build,runtime}-dependency on libmodule-runtime-perl to >= 0.014.
  * Add build-dependency on libmoose-perl, to enable optional tests.
  * Use https:// for the upstream Git repository metadata.
  * Declare compliance with Standards-Version 3.9.6.
  * Update debian/copyright accordingly to upstream-provided information.

 -- intrigeri <intrigeri@debian.org>  Sat, 23 May 2015 16:47:28 +0200

libmoox-types-mooselike-perl (0.27-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ intrigeri ]
  * Add debian/upstream/metadata
  * New upstream release.
  * Declare compliance with Standards-Version 3.9.5 (no change required).

 -- intrigeri <intrigeri@debian.org>  Tue, 26 Aug 2014 13:18:19 -0700

libmoox-types-mooselike-perl (0.25-2) unstable; urgency=low

  * Fix typo in debian/NEWS. Thanks to David Bremner for spotting.
  * Move libmoox-types-mooselike-numeric-perl and libmoox-types-setobject-
    perl to Suggests. The only reverse dependency is already fixed. Thanks
    to Bill Allombert for the bug report. (Closes: #722506)

 -- gregor herrmann <gregoa@debian.org>  Sat, 14 Sep 2013 15:10:49 +0200

libmoox-types-mooselike-perl (0.25-1) unstable; urgency=low

  [ Jonas Smedegaard ]
  * Fix tighten (build-)dependency on Moo: Version 0.09101 cause FTBFS
    at testsuite (other versions << 1.000001 not tested).

  [ gregor herrmann ]
  * New upstream release.
  * Add entry to debian/NEWS about split off modules.

 -- gregor herrmann <gregoa@debian.org>  Sun, 11 Aug 2013 09:30:19 +0200

libmoox-types-mooselike-perl (0.21-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)

  [ gregor herrmann ]
  * New upstream release.
  * Update copyright years and holders.
  * Add debian/NEWS mentioning an API change.

 -- gregor herrmann <gregoa@debian.org>  Sat, 02 Mar 2013 20:26:35 +0100

libmoox-types-mooselike-perl (0.16-1) unstable; urgency=low

  * Initial release (closes: #696098).

 -- gregor herrmann <gregoa@debian.org>  Sun, 16 Dec 2012 18:52:36 +0100
